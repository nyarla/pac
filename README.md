pac
===

Pac Ain't Container - A [Archlinux](https://www.archlinux.org/) [proot](http://proot.me/) jailing script without root privilege.

DESCRIPTION
-----------

This script is written for bootstrap and manage [proot](http://proot.me/) jailing environment with [Archlinux](https://www.archlinux.org/).

REQUIREMENTS
------------

  * running on linux
  * perl5
  * bash
  * curl or wget
  * tar
  * xz

USAGE
-----

```bash
# initializing first time
$ pac init

# build archlinux jailing without root privilege.
# this trick provided by `proot`.
$ pac bootstrap --name dev

# run command on Archlinux proot jail.
$ pac exec --name dev -- /bin/echo hello

# remove Archlinux proot jail environment
$ pac rm --name dev
```

COPYRIGHTS
----------

Building for Archlinux proot jailing is based on [tokland/arch-bootstrap](https://github.com/tokland/arch-bootstrap),
and this script is under the [MIT](https://github.com/tokland/arch-bootstrap#license) license.

Author
------

Naoki OKAMUAR (Nyarla) <nyarla@thotep.net>

LICENSE
-------

MIT

