#!/usr/bin/env perl

use strict;
use warnings;

use feature qw[ state switch ];
no warnings 'experimental';

use File::Spec;
use Getopt::Long;
use Pod::Usage qw[ pod2usage ];

our $VERSION = '0.02';

sub true()            { return !! 1 }
sub false()           { return !! 0 }
sub HOME()            { return state $dir = $ENV{'HOME'} }
sub ROOT_DIR()        { return state $dir = $ENV{'PAC_ROOT_DIR'}        // HOME . '/.pac.d'         }
sub CONTAINERS_DIR()  { return state $dir = $ENV{'PAC_CONTAINERS_DIR'}  // ROOT_DIR . '/containers' }
sub STATE_DIR()       { return state $dir = $ENV{'PAC_STATE_DIR'}       // ROOT_DIR . '/states'     }
sub BINARY_DIR()      { return state $dir = $ENV{'PAC_BINARY_DIR'}      // ROOT_DIR . '/bin'        }
sub DOWNLOAD_DIR()    { return state $dir = $ENV{'PAC_DOWNLOAD_DIR'}    // ROOT_DIR . '/downloads'  }

sub color($$) {
  state $ink //= {
    start   => "\033[",
    end     => "\033[0m",
    black   => '0;30m',
    red     => '1;31m',
    green   => '1;32m',
    yellow  => '1;33m',
    blue    => '1;34m',
    purple  => '1;35m',
    cyan    => '1;36m',
    gray    => '0;37m',
    white   => "\033[0m",
  };

  my $name  = shift @_ || die "color name is not specified.";
  my $text  = shift @_ || die "message body is not specified.";

  my $start = $ink->{'start'};
  my $end   = $ink->{'end'};

  if ( ! exists $ink->{$name} ) {
    die "invalid color name: ${name}";
  }

  my $color = $ink->{$name};

  return join(q{}, ($start, $color, $text, $end));
}

sub subject($) {
  my $text    = shift;
  my $prefix  = color 'green', '==>';

  print "${prefix} ${text}\n";
}

sub action($) {
  my $text    = shift;
  my $prefix  = color 'blue', '-->';

  print "${prefix} ${text}\n";
}

sub err($) {
  my $text    = shift;
  my $prefix  = color 'red', '!!!';

  print "${prefix} ${text}\n";
  exit 1;
}

sub workdir($$) {
  my ( $type, $name ) = @_;
  my $base;

  for ( $type ) {
    when (/^containers$/)   { $base = CONTAINERS_DIR  }
    when (/^state$/)        { $base = STATE_DIR       }
  }

  $base = File::Spec->rel2abs($base);

  my $dir   = File::Spec->catdir($base, $name);
  my $check = quotemeta($dir);

  if ( $dir !~ m{^$check} ) {
    err "directory traversal is deleted: ${base}/${name} => ${dir}";
  }

  return $dir;
}


sub cmd(@) {
  return system(@_) == 0;
}

sub bash($) {
  return cmd 'bash', '-c', $_[0];
}

sub capture($) {
  my $cmd = shift;
  my $ret = `${cmd}`;

  chomp($ret);

  return $ret;
}

sub has($) {
  my $cmd = shift;
  return cmd('which', $cmd); 
}

sub arch() {
  return capture('uname -m');
}

sub uncompress($$) {
  my ( $file, $dest ) = @_;

  if ( $file =~ m{\.gz$} ) {
    return cmd 'tar', 'zxf', $file, '-C', $dest
      || err "failed to extract files from archive: ${file}: ${!}";
  }
  elsif ( $file =~ m{\.xz$} ) {
    return bash "xz -dc '${file}' | tar x -C '${dest}'"
      || err "failed to extract files from archive: ${file}: ${!}";
  }

  err "Unsupported archive format: ${file}";
}

sub agent($;$) {
  state $agent //= ( has 'curl' )
    ? __PACKAGE__->can('agent_curl')
    : ( has 'wget' )  ? __PACKAGE__->can('agent_wget')
                      : err "curl or wget command is not found." ;

  return $agent->(@_);
}

sub agent_curl {
  my $url = shift @_;

  if ( @_ == 1 ) {
    my $dest = shift @_;
    return cmd 'curl', '-L', '-s', $url, '-o', $dest;
  }

  return capture("curl -L -s ${url}");
}

sub agent_wget {
  my $url = shift @_;

  if ( @_ == 1 ) {
    my $dest = shift @_;
    return cmd 'wget', '-O', $dest, $url;
  }

  return capture("wget ${url}");
}

sub download($$) {
  return agent($_[0], $_[1]);
}

sub fetch($) {
  return agent($_[0]);
}

sub specs(@) {
  state $spec //= {
    'x86_64'  => {
      repo          => 'http://mirrors.kernel.org/archlinux',
      proot         => 'https://raw.githubusercontent.com/proot-me/proot-static-build/master/static/proot-x86_64',
      proot_sha1sum => '5e713559fd336074971590e8cbe7ab1593ffabfc',
      },
    'i686'    => {
      repo          => 'http://mirrors.kernel.org/archlinux',
      proot         => 'https://raw.githubusercontent.com/proot-me/proot-static-build/master/static/proot-x86',
      proot_sha1sum => '6b2f18ff13c1b7f1efe5b55dac0dadaa6966f7a1',
    },
    'arm'     => {
      repo          => 'http://mirror.archlinuxarm.org',
      proot         => 'https://raw.githubusercontent.com/proot-me/proot-static-build/master/static/proot-arm',
      proot_sha1sum => '1c75e34aaa5e018aaa74f6d220072b6573daecdf',
    },
    'arm64'   => {
      repo          => 'http://mirror.archlinuxarm.org',
      proot         => 'https://raw.githubusercontent.com/proot-me/proot-static-build/master/static/proot-arm64',
      proot_sha1sum => '863c9aa975c3a3258bf63dfe7d83654887306173',
    }
  };

  my ($arch, $name);

  if (@_ == 1) {
    $arch = arch();
    $name = shift @_;
  } elsif ( @_ > 1) { 
    ( $arch, $name ) = @_;
  } else {
    err "internal error: spec: arch or data section name is not specified.";
  }

  if ( ! exists $spec->{$arch} ) {
    err "unsupported architecture: ${arch}";
  }

  if ( ! exists $spec->{$arch}->{$name} ) {
    err "wrong data section name on ${arch} arch: ${name}";
  }

  return $spec->{$arch}->{$name};
}

sub packages($) {
  state $spec //= {
    pacman => [qw<
      acl archlinux-keyring attr bzip2 curl expat glibc gpgme libarchive
      libassuan libgpg-error libssh2 lzo openssl pacman pacman-mirrorlist xz zlib
      krb5 e2fsprogs keyutils libidn gcc-libs
    >],
    basic => [qw< filesystem >],
    extra => [qw< coreutils bash grep gawk file tar sed >],
  };

  my $name = shift @_ || err "inernal error: package section is not specified.";
  if ( ! exists $spec->{$name} ) {
    err "wrong package section name: ${name}";
  }

  return @{ $spec->{$name} };
}

sub repo(;$) {
  my $arch = arch;
  my $type = shift @_ // 'url';

  if ( $arch =~ m{arm} ) {
    return ( $type eq 'url' )
              ? spec($arch, 'repo') . '/' . $arch . '/core'
              : spec($arch, 'repo') . '/' . $arch ;
  }

  return ( $type eq 'url' )
            ? specs($arch, 'repo') . '/core/os/' . $arch
            : specs($arch, 'repo') . '/$repo/os/$arch' ;
}

sub package_list() {
  my $url   = repo;
  my @list  = ( fetch($url) =~ m{<a [^>]*href="([^"]*)".*>}g );

  return sort { $a cmp $b }
         map  { my @s = split(m{/}, $_); pop @s } @list;
}

sub container_init_minimal {
  my $name  = shift @_;
  my $dest  = workdir 'containers', $name;
  my $state = workdir 'state', $name;
  my $down  = DOWNLOAD_DIR;
  my $repo  = repo;
  my @list  = package_list;

  subject 'Pre-initialization for proot jail with Archlinux';

  action "create proot jail directory: ${dest}";
  cmd 'mkdir', '-p', $dest
    || err "failed to create proot jail directroy: ${dest}";

  action "create proot jail state directory: ${state}";
  cmd 'mkdir', '-p', $state
    || err "failed to create state directory for proot jail: ${state}";

  for my $pkg ( packages('pacman'), packages('basic') ) {
    action "fetch package archive: ${pkg}";
    
    my $file = ( grep { $_ =~ m{${pkg}-.*\.tar\.(?:gz|xz)$} } @list )[0];
    if ( ! defined($file) ) {
      err "cannot find package from package list: ${pkg}";
    }

    download "${repo}/${file}", "${down}/${file}";

    action "extract archive: ${pkg}";
    uncompress "${down}/${file}", $dest;
  }
}

sub container_init_pacman {
  my $name = shift;
  my $dest = workdir 'containers', $name;
  my $repo = repo('template');

  subject 'Configure DNS';
  action 'copy /etc/resolve.conf';

  cmd 'cp', '/etc/resolv.conf', "${dest}/etc/resolv.conf"
    || err "failed to copy /etc/resolve.conf to proot jail with Archlinux";

  subject 'Configure pacman repository';
  action 'set pacman mirror';
  
  bash "echo 'Server = ${repo}' >> ${dest}/etc/pacman.d/mirrorlist"
    || err "failed to configure pacman mirrorlist: ${!}";
}

sub container_init {
  my $name = shift;
  my $dest = workdir 'containers', $name;

  subject 'Initialize for proot jail with Archlinux';
  
  action 'generate /etc/passwd';
  bash "echo 'root:x:0:0:root:/root:/bin/bash' >${dest}/etc/passwd"
    || err "failed to generate /etc/passwd";

  action 'generate /etc/shadow';
  bash "echo 'root:\$1\$GT9AUpJe\$oXANVIjIzcnmOpY07iaGi/:14657::::::' >${dest}/etc/shadow"
    || err "failed to generate /etc/shadow";
  
  if ( ! -e "${dest}/etc/mtab" ) {
    action 'generate /etc/mtab';
    bash "echo 'rootfs / rootfs rs 0 0' >${dest}/etc/mtab"
      || err "failed to generate /etc/mtab: ${!}";
  }

  action 'quick fix to /etc/pacman.conf for bootstrap pacman';
  bash 'sed -i "s/^[[:space:]]*\(CheckSpace\)/# \1/" "'.$dest.'/etc/pacman.conf" && sed -i "s/^[[:space:]]*SigLevel[[:space:]]*=.*$/SigLevel = Never/" "'.$dest.'/etc/pacman.conf"'
    || err "failed to quick fix for pacman.conf: ${!}";
}

sub container_exec {
  my @root  = ((shift @_) eq 'root' ) ? ( '-0' ) : () ;
  my $name  = shift @_;
  my $dest  = workdir 'containers', $name;
  my $state = workdir 'state', $name;
  my @cmds  = @_;

  bash "test -d '${dest}' && test -d '${state}'"
    || err "directory for proot jail is missing.";

  cmd('env', '-i', 'LC_ALL=C',
      BINARY_DIR.'/proot', @root,
        '-m', '/dev', '-m', '/sys', '-m', '/proc',
        '-b', "${state}:/opt/data",
        '-w', '/',
        '-r', $dest,
        @cmds)
    || err "failed to exec command: @{[ join(q{ }, @cmds) ]}: ${!}";
}

sub container_exec_as_user { return container_exec('user', @_) }
sub container_exec_as_root { return container_exec('root', @_) }

sub pac_init {
  my @args = @_;
  
  subject 'Initialization for pac';
  
  my @tasks = (
    root        => ROOT_DIR,
    containers  => CONTAINERS_DIR,
    state       => STATE_DIR,
    binary      => BINARY_DIR,
    download    => DOWNLOAD_DIR,
  );

  while ( my ( $name, $dir ) = splice @tasks, 0, 2  ) {
    if ( ! -d $dir ) {
      action "create ${name} directory: ${dir}";
      cmd 'mkdir', '-p', $dir
        || err "failed to create ${name} directory: ${dir}: ${!}";
    }
  }

  my $proot = BINARY_DIR . '/proot';

  if ( ! -e $proot ) {
    my $arch = lc(arch);
    my $bin  = specs($arch, 'proot');
    my $hash = specs($arch, 'proot_sha1sum');

    action "downloading 'proot' binary for ${arch} from ${bin}";
    download($bin, $proot)
      || err "failed to download binary from ${bin}: ${!}";

    action "checking for sha1sum: expect -> ${hash}";
    my $sha1sum = ( capture('sha1sum -b ' . BINARY_DIR . '/proot') =~ m{^([0-9a-f]+)} )[0];

    action "checking for sha1sum: got    -> ${sha1sum}";
    if ( $sha1sum ne $hash ) {
      unlink BINARY_DIR . '/proot';
      err "checksum of 'proot' binary does not match. initialization aborted.";
    } else {
      action 'chmod 500 proot';
      cmd 'chmod', '500', BINARY_DIR . '/proot'
        || err "failed to chmod 500 " . BINARY_DIR . '/proot' . ": ${!}";
    }
  }

  action "Done";
}

sub action_init {
  pac_init();
}

sub action_bootstrap {
  my $name = shift;

  if ( ! defined($name) || $name eq q{} ) {
    err "proot jail name is not specified.";
  }

  if ( -e workdir('containers', $name) ) {
    err "proot jail of ${name} already exists.";
  }

  container_init_minimal($name);
  container_init_pacman($name);
  container_init($name);

  container_exec_as_root(
    $name,
    '/usr/bin/pacman',
      '--noconfirm', '-Sy', '--force',
      (packages('pacman'), packages('basic'), packages('extra'))
  );

  container_init_pacman($name);

  subject 'Done.';
}

sub action_exec {
  my $name    = shift @_;
  my $as_root = shift @_;

  if ( ! defined($name) || $name eq q{} ) {
    err "proot jail name is not specified.";
  }

  return ( !! $as_root )
    ? container_exec_as_root($name, @_)
    : container_exec_as_user($name, @_) ;
}

sub action_rm {
  my $name = shift;
 
  if ( ! defined($name) || $name eq q{} ) {
    err "proot jail name is not specified.";
  }

  my $dest  = workdir 'containers', $name;
  my $state = workdir 'state', $name;

  for my $dir ( $dest, $state ) {
    bash "test -d '${dir}' && chmod -R 700 '${dir}' && rm -rf '${dir}'"
      || err "failed to remove directory for proot jail: ${dir}: ${!}";
  }
}

sub main {
  local @ARGV = @ARGV;

  my $command = shift @ARGV;
  my ( $name, $as_root );

  GetOptions(
    'name=s'  => \$name,
    'root!'   => \$as_root,
    'help'    => sub {
      pod2usage(0);
    },
    'version'  => sub {
      print "${VERSION}\n";
      exit 0;
    },
  ) || exit(1);

  for ( $command ) {
    when (/^init$/)           { action_init()                             }
    when (/^bootstrap$/)      { action_bootstrap($name)                   }
    when (/^exec$/)           { action_exec($name, !! $as_root, @ARGV)    }
    when (/^rm$/)             { action_rm($name)                          }
    when (/^(?:--)?help$/)    { pod2usage(-verbose => 99, -exitval => 0)  }
    when (/^(?:--)?usage$/)   { pod2usage(0)                              }
    default                   { err "unknown command: ${command}"         }
  }
}

sub testing {
  require Test::More;
  Test::More->import;
}

if ( ! $ENV{'HARNESS_ACTIVE'} ) {
  main();
} else {
  testing();
}

__END__

=head1 NAME

pac - PAC Ain't Container

=head1 SYNOPSIS

  pac - Pac Ain't Container

  pac <command> [options...]

  Commands:
    init        initialize pac environment
    bootstrap   bootstrap proot jail
    exec        runnning command on proot jail
    rm          remove proot jail envronment
    usage       display usage message
    help        display help message
    version     display version

  Options:
    --name <name> the name of proot jail.
    --root        the flag for runnning command as (pseudo/emulated) root.

=head1 COMMANDS

=head2 init

  $ pac init

This command creates working directory and download C<proot> binary for using pac.

=head2 bootstrap

  $ pac bootstrap --name dev

This command bootstraps  proot jail environment.

=head2 exec

  $ pac exec --name dev [--root] -- <command>

This command execute C<command> in proot jail environment.

=head2 rm

  $ pac rm --name dev

This command removes proot jail environment.

=head2 help

  $ pac help

This command displays help.

=head1 OPTIONS

=head2 C<--name>

The name of proot jail.

This options is required at C<bootstrap>, C<exec> and C<rm> command.

=head2 C<--root>

The flag for running command as (pseudo/emulated) root.

This option is switch of C<-0> flag for calling C<proot> command.

Default value of this options is C<false>,
So command running as user privilege by default.

=head1 DESCRIPTION

This command creates Archlinux jail environment without root privilege.

This command uses C<proot> (L<http://proot.me/>) for creates jail environment without root privilege.

=head1 COPYRIGHTS

Building for Archlinux proot jailing is based on L<https://github.com/tokland/arch-bootstrap>,
and this script is under the MIT license (L<https://github.com/tokland/arch-bootstrap#license>).

=head1 AUTHOR

Naoki OKAMURA (Nyarla) E<lt>nyarla@thotep.netE<gt>

=head1 LICENSE

MIT

=head1 SEE ALSO

L<http://proot.me>

L<https://github.com/tokland/arch-bootstrap>

L<https://github.com/nyarla/pac>

=cut
